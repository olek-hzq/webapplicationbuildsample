﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using WebApplicationBuildSample.Infrastructure;

namespace WebApplicationBuildSample.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<SelectListItem> customers = new List<SelectListItem>();
            using (var dbContext = new ObjectDbContext())
            {
                customers = dbContext.Customers.Select(x => new SelectListItem
                {
                    Value = x.Id.ToString(),
                    Text = x.Name
                }).ToList();
            }

            customers.Insert(0, new SelectListItem
            {
                Value = "0",
                Text = @"---"
            });

            ViewBag.CustomerList = customers;

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}