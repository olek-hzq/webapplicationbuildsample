﻿using System.Data.Entity;
using WebApplicationBuildSample.Domain;

namespace WebApplicationBuildSample.Infrastructure
{
    public class ObjectDbContext : DbContext
    {
        public ObjectDbContext() : base("DefaultConnection")
        {

        }

        public IDbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>().HasKey(x => x.Id);
            modelBuilder.Entity<Customer>().Property(x => x.Name).IsRequired().HasMaxLength(32);

            base.OnModelCreating(modelBuilder);
        }
    }
}