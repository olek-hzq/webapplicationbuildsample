﻿using System;

namespace WebApplicationBuildSample.Domain
{
    public class Customer
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime CreationTime { get; set; }
    }
}